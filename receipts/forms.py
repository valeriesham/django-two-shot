from django import forms
from receipts.models import Receipt, ExpenseCategory, Account
from django.forms.widgets import NumberInput


class ReceiptForm(forms.ModelForm):
    date = forms.DateField(
        widget=NumberInput(attrs={"type": "date"}), label=""
    )
    category = forms.ModelChoiceField(
        queryset=ExpenseCategory.objects.all(),
        empty_label="Select the Category",
        label="",
    )
    account = forms.ModelChoiceField(
        queryset=Account.objects.all(),
        empty_label="Select the Account",
        label="",
    )

    class Meta:
        model = Receipt
        exclude = ["purchaser"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["vendor"].label = ""
        self.fields["vendor"].widget.attrs.update(
            {"placeholder": "Receipt vendor"}
        )
        self.fields["total"].label = ""
        self.fields["total"].widget.attrs.update(
            {"placeholder": "Receipt total"}
        )
        self.fields["tax"].label = ""
        self.fields["tax"].widget.attrs.update({"placeholder": "Receipt tax"})


class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        exclude = ["owner"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update(
            {"placeholder": "Category name"}
        )
        self.fields["name"].label = ""


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        exclude = ["owner"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update(
            {"placeholder": "Account name"}
        )
        self.fields["name"].label = ""

        self.fields["number"].widget.attrs.update(
            {"placeholder": "Account number"}
        )
        self.fields["number"].label = ""
